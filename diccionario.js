let game1 = {name: "The Prince of Persia: The Sands of Time", year: 2003, genre: "Platforms"}

let game2 = {name: "The Prince of Persia: Warrior Within", year: 2004, genre: "Platforms"}

let game3 = {name: "The Prince of Persia: The Two Thrones", year: 2005, genre: "Platforms"}

let game4 = {name: "God of War", year: 2005, genre: "Actions", subgenre: "Platforms"}

let game5 = {name: "God of War II", year: 2007, genre: "Actions"}

let game6 = {name: "God of War III", year: 2008, genre: "Actions"}

let game7 = {name: "Assassins Creed", year: 2007, genre: "Stealth"} 

let game8 = {name: "Assassins Creed II", year: 2009, genre: "Stealth"}

let game9 = {name: "Assassins Creed: Brotherhood", year: 2010, genre: "Stealth"}

let game10 = {name: "Assassins Creed: Revelations", year: 2011, genre: "Stealth", subgenre: "Platforms"}


let ubisoft = [game1, game2, game3, game4, game5, game6, game7, game8, game9, game10]


console.log(ubisoft.filter(game => game.genre === "Stealth",));
console.log(ubisoft.filter(game => game.subgenre === "Platforms"));


